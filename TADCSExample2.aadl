package TADCSExample2
public
	--with Base_Types;
	
	system Door_sensor
		features
			door_open_s: out data port;
			door_close_s: out data port;
			
			door_state_s: in data port;
			door_close_on_person: out data port;
		
	end Door_sensor;
	
	system implementation Door_sensor.impl
		annex asam {**
			--Identify type of major component
			type => sensor;
			--Door sensor has an internal failure causes error(s)on out ports.
			internal failure INTF1: "door report it is closed on a person in the doorway" causes [DoorSensorCommissionError(Critical,p=0.02)] on [door_close_on_person, door_open_s, door_close_s];
			--Door sensor has a safety constraint to prevent error out.
			safety constraint SC1: "doors should be opened when the person is in the doorway" prevents [DoorSensorCommissionError] on [door_open_s, door_close_s] verified by [GAU1];
		**};
		
	end Door_sensor.impl;
	
	system Door_controller
		features
			door_open_c: in data port;
			door_close_c: in data port;
			open_door_cmd: out data port; 
			close_door_cmd: out data port;
			door_state_c: in data port;
			door_open_train_not_stopped: out data port;
	end Door_controller;
	
	system implementation Door_controller.impl
		annex asam {**
			--Identify type of major component
			type => controller;
			--Door controller has internal failure causes error(s) on out ports.
			internal failure INTF2: "door report it is opened when the train is not stopped" causes [DoorControllerCommissionError(Marginal,p=0.03)] on [door_open_train_not_stopped, close_door_cmd, open_door_cmd];
			--Error comes from door sensor and causes error on controller's output ports.
			[DoorSensorCommissionError] on [door_open_c, door_close_c, door_state_c] causes [DoorSensorCommissionError(Catastrophic,p=0.002)] on [door_open_train_not_stopped, close_door_cmd, open_door_cmd];
			--Door controller have safety constraints to handle incoming error and prevent outgoing error.
			safety constraint SC2:"doors should be opened when the person is in the doorway" handles [DoorSensorCommissionError] on [door_open_c, door_close_c, door_state_c] verified by [GAU1];
			safety constraint SC3:"doors should be closed when the train is not stopped at the station platform" prevents [DoorControllerCommissionError]on [close_door_cmd, open_door_cmd] verified by [GAU1];
		**};
	end Door_controller.impl;
	
	system Door_actuator
		features
			open_door_a: in data port;
			close_door_a: in data port;
			
			door_state_a: in data port;
			door_blocked_in_emergency: out data port;
	end Door_actuator;
	
	system implementation Door_actuator.impl
		annex asam {**
			--Identify type of major component
			type => actuator;
			--Door actuator has internal failure causes error on out ports
			internal failure INTF3: "door report it is blocked during emergency" causes [DoorActuatorCommissionError(Catastrophic,p=0.04)] on [door_blocked_in_emergency];--outputport
			--Error comes from door controller and causes error on out port.
			[DoorControllerCommissionError] on [open_door_a, close_door_a, door_state_a] causes [DoorControllerCommissionError(Critical,p=0.1)] on [door_blocked_in_emergency];--input->output
			-- Door actuator have safety constraints to handle incoming errors and prevent outgoing errors.
			safety constraint SC4:"doors should be closed before train is moving and doors should be opened after train is stopped at the station platform" handles [DoorControllerCommissionError] on [open_door_a,close_door_a, door_state_a] verified by [GAU1];
			--safety constraint SC5:"doors should be opened during emergency" prevents [DoorActuatorCommissionError]on [door_blocked_in_emergency] verified by [GAU1];
		**};
	end Door_actuator.impl;
	
	system Physical_Door
		features
			door_state_in_cp: in data port ;
			door_state_out_cp: out data port;
	end Physical_Door;
	
	system implementation Physical_Door.impl
		annex asam {**
			type => controlled_process;
			[DoorControllerCommissionError] on [door_state_in_cp] causes [DoorControllerCommissionError(Marginal,p=0.01)] on [door_state_out_cp];--outputport
			[DoorActuatorCommissionError] on [door_state_in_cp] causes [DoorActuatorCommissionError(Negligible,p=0.00001)] on [door_state_out_cp];--in->out
		**};
	end Physical_Door.impl;
	
	system entire_system	
	end entire_system;
	
	system implementation entire_system.impl
		subcomponents
			Door_sensor: system Door_sensor.impl;
			Door_controller: system Door_controller.impl;
			Door_actuator: system Door_actuator.impl;
			Physical_Door: system Physical_Door.impl;
		connections
			conn_1: port Door_sensor.door_open_s -> Door_controller.door_open_c;
			--conn_2: port Door_sensor.door_blocked_s -> Door_controller.door_blocked_c;
			conn_3: port Door_controller.open_door_cmd -> Door_actuator.open_door_a;
			conn_4: port Door_controller.close_door_cmd -> Door_actuator.close_door_a;
			--conn_5: port Door_actuator.door_state_a -> Physical_Door.door_state_in_cp;
			conn_6: port Physical_Door.door_state_out_cp -> Door_sensor.door_state_s;
			conn_7: port Door_sensor.door_close_s -> Door_controller.door_close_c;
			conn_8: port Door_sensor.door_close_on_person ->Door_controller.door_state_c;
			conn_9: port Door_controller.door_open_train_not_stopped -> Door_actuator.door_state_a;
			conn_10: port Door_actuator.door_blocked_in_emergency->Physical_Door.door_state_in_cp;
	end entire_system.impl;
	
end TADCSExample2;
