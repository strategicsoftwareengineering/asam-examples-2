package PMExample2
public
	system Electrode
		features
			Measure_Sensing: out event data port;
			Heart_Response: in event data port;
	end Electrode;
	
	system implementation Electrode.impl
		annex asam{**
			--Identify type of major component
			type => sensor;
			--Electrode is the sensor of the pacemaker. The internal failure in it causes ElectrodeError.
			internal failure INTF1:"The electrode does not measure sensing values on time" causes [LateMeasureSensing(Critical,p=0.02)] on [Measure_Sensing];
		**};
	end Electrode.impl;
	
	system Device_Controller_Monitor
		features
			Heart_Monitoring: in event data port;
			Send_Pulse_Command: out event data port;
	end Device_Controller_Monitor;
	
	system implementation Device_Controller_Monitor.impl
		annex asam{**
			--Identify type of major component
			type => controller;
			--DCM is the controller of pacemaker. The internal failure in it causes DCM error.
			internal failure INTF2:"The DCM does not send pacing command on time to the pulse generator" causes [LatePacingCommand(Critical,p=0.03)] on [Send_Pulse_Command];
			--The ElectrodeError propagates to DCM.
			[LateMeasureSensing] on [Heart_Monitoring] causes [LateMeasureSensing(Critical,p=0.002)] on [Send_Pulse_Command];
			--DCM have a SC to mitigate errors coming from the electrode
			safety constraint SC1:"The DCM should receive heart sensing values on time to send the pace command on time as well" handles [LateMeasureSensing] on [Heart_Monitoring] verified by [GUA1];
		**};
	end Device_Controller_Monitor.impl;
	
	system Pulse_Generator
		features
			Receive_Pulse_Command: in event data port;
			Heart_Pacing: out event data port;
	end Pulse_Generator;
	
	system implementation Pulse_Generator.impl
		annex asam{**
			--Identify type of major component
			type => actuator;
			--The PG is the pulse generator. The internal failure in it causes PG erros.
			internal failure INTF3:"The pulse generator does not generate the required pulse whenever is needed" causes [DelayedPacingService(Critical,p=0.04)] on [Heart_Pacing];
			--DCMError from controller causes DelayedService in PG.
			[LatePacingCommand] on [Receive_Pulse_Command] causes [LatePacingCommand(Critical,p=0.1)] on [Heart_Pacing];
			safety constraint SC2:"The pulse generator should receive the pacing command on time from DCM" handles [LatePacingCommand] on [Receive_Pulse_Command] verified by [GUA1];
			--safety constraint SC3:"The pulse generator should not provide the late pacing service to the heart" prevents [DelayedPacingService]on [Heart_Pacing] verified by [GUA1];
		**};
	end Pulse_Generator.impl;
	
	system Heart
		features
			Heart_Received_Pulse: in event data port;
			Heart_Reponsed_Pulse: out event data port;
	end Heart;
	
	system implementation Heart.impl
		annex asam{**
			--Identify type of major component.
			type=> controlled_process;
			--DCMError causes LatePacingCommand in the Heart if it not mitigated by PG.
			[LatePacingCommand] on [Heart_Received_Pulse] causes [LatePacingCommand(Critical,p=0.03)] on [Heart_Reponsed_Pulse];
			--PulseGeneratorError causes DelayedPacingService start in the Heart.It can be mitigated by PG itself to don't hurt the heart.
			[DelayedPacingService] on [Heart_Received_Pulse] causes [DelayedPacingService(Critical,p=0.04)] on [Heart_Reponsed_Pulse];
		**};
	end Heart.impl;
	
	system Entire_PM
	end Entire_PM;
	
	system implementation Entire_PM.impl
		subcomponents
			ELECTRODE: system Electrode.impl;
			DEVICE_CONTROLLER_MONITOR: system Device_Controller_Monitor.impl;
			PULSE_GENERATOR: system Pulse_Generator.impl;
			HEART: system Heart.impl;
		connections
			c0: port ELECTRODE.Measure_Sensing->DEVICE_CONTROLLER_MONITOR.Heart_Monitoring;
			c1: port DEVICE_CONTROLLER_MONITOR.Send_Pulse_Command->PULSE_GENERATOR.Receive_Pulse_Command;
			c2: port PULSE_GENERATOR.Heart_Pacing->HEART.Heart_Received_Pulse;
			c3: port HEART.Heart_Reponsed_Pulse->ELECTRODE.Heart_Response;
	end Entire_PM.impl;
end PMExample2;
